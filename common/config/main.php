<?php

$db_local = (file_exists(__DIR__ . '/db-local.php'))?require(__DIR__ . '/db-local.php'):[];

$db = array_merge(
    require(__DIR__ . '/db.php'),
    $db_local
);

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
		'db' => $db,
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
		'urlManager' => [
			'enablePrettyUrl' => true,
		]
    ],
	'modules' => [

	],
];

