<?php
$common_params = file_exists(__DIR__ . '/../../common/config/params-local.php')?require(__DIR__ . '/../../common/config/params-local.php'):[];
$native_params = file_exists(__DIR__ . '/params-local.php')?require(__DIR__ . '/params-local.php'):[];

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php'),
    $common_params,
	$native_params
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
		'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gVUgFxZc1hdMaX32tkhArdWTw5E9ENPN',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'default/error',
        ],
    ],
	'modules' => [

	],
    'params' => $params,
];
