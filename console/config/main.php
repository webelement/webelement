<?php

$common_params = file_exists(__DIR__ . '/../../common/config/params-local.php')?require(__DIR__ . '/../../common/config/params-local.php'):[];
$native_params = file_exists(__DIR__ . '/params-local.php')?require(__DIR__ . '/params-local.php'):[];

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php'),
    $common_params,
	$native_params
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
