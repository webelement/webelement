<?php
namespace backend\components;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller
 */
class BaseBackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'error'],
                        'allow' => true,
						'roles' => ['@'],
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
	
	public function actionCreate()
    {
        return $this->render('create');
    }
	
	public function actionUpdate()
    {
        return $this->render('update');
    }
	
	public function actionView()
    {
        return $this->render('view');
    }
	
	public function actionDelete()
    {
        return $this->render('delete');
    }

}
