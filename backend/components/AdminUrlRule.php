<?php
namespace backend\components;

use yii\web\UrlRuleInterface;
use yii\base\Object;

class AdminUrlRule extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
		// сюда еще доберусь
        if ($route === 'car/index') {
            if (isset($params['manufacturer'], $params['model'])) {
                return $params['manufacturer'] . '/' . $params['model'];
            } elseif (isset($params['manufacturer'])) {
                return $params['manufacturer'];
            }
        }
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
		if (strpos($request->getUrl(),'?') !== false) {
			list($route, $params) = explode('?',$request->getUrl());
		} else {
			$route = $request->getUrl();
			$params = '';
		}
		parse_str($params, $params);
		if (strpos('/params/',$request->getUrl()) !== false) {
			list($route, $params_g) = explode('/params/', $route);
			$params_g = explode('/', $params_g);
			for ($i=0; $i < count($params_g); $i++) {
				$params[$params_g[$i]] = $params_g[$i+1];
				$_GET[$params_g[$i]] = $params_g[$i+1];
				$_REQUEST[$params_g[$i]] = $params_g[$i+1];
				$i++;
			}
		}
		$route = trim(str_replace('admin','',$route),'/');
		if (empty($route)) {
			return ['default/index', $params];
		}
		$route = explode('/', $route);
		$path = array();
		foreach ($route as $key => $elem) {
			$module_dir = '/modules/';
			if (count($path) > 1) {
				$module_dir.= implode('/modules/',$path).'/modules/'.$elem;
			} else if (!empty($path)) {
				$module_dir.= $path[0].'/modules/'.$elem;
			} else {
				$module_dir.= $elem;
			}
			if (is_dir('..'.$module_dir) or is_dir('../../common'.$module_dir)) { 
				$path[] = $elem; 
				unset($route[$key]);
			} else {
				break;
			}
		}
		$route = array_values($route);
		if (count($route) > 2) {
			return false;
		}
		if (empty($route)) {
			$path[] = 'default';
			$path[] = 'index';
		} else if (count($route) == 1) {
			$path[] = $route[0];
			$path[] = 'index';
		} else {
			$path[] = $route[0];
			$path[] = $route[1];
		}
		return [implode('/',$path), $params];
    }
}