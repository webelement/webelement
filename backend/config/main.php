<?php
$common_params = file_exists(__DIR__ . '/../../common/config/params-local.php')?require(__DIR__ . '/../../common/config/params-local.php'):[];
$native_params = file_exists(__DIR__ . '/params-local.php')?require(__DIR__ . '/params-local.php'):[];

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php'),
    $common_params,
	$native_params
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
		'assetManager' => [
			'bundles' => [
				'dmstr\web\AdminLteAsset' => [
					'skin' => 'skin-red',
				],
			],
		],
		'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hZx5ElA1WN18Y6-0byv7_DlsqnKEVPDY',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'default/error',
        ],
		'urlManager' => [
            'enablePrettyUrl' => true,
			'showScriptName' => false,
            'enableStrictParsing' => true,
			'rules'=> [
				['class' => 'backend\components\AdminUrlRule'],
			]
		]
    ],
	'modules' => [

	],
    'params' => $params,
];
