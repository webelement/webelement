<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage(); 
	if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-red sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
			<?= Html::a('<span class="logo-mini">Сайт</span><span class="logo-lg">На сайт</span>','/', ['class' => 'logo']) ?>
			<nav class="navbar navbar-static-top" role="navigation">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
						</li>
						<li class="dropdown user user-menu">
							<?= Html::a('Выйти',['/site/logout'],['data-method' => 'post']) ?>
						</li>
					</ul>
				</div>
			</nav>
		</header>
        <aside class="main-sidebar">
			<section class="sidebar">
				<?= dmstr\widgets\Menu::widget(
					[
						'options' => ['class' => 'sidebar-menu'],
						'items' => [
							['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
							['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
							['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
							['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
							[
								'label' => 'Same tools',
								'icon' => 'fa fa-share',
								'url' => '#',
								'items' => [
									['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
									['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
									[
										'label' => 'Level One',
										'icon' => 'fa fa-circle-o',
										'url' => '#',
										'items' => [
											['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
											[
												'label' => 'Level Two',
												'icon' => 'fa fa-circle-o',
												'url' => '#',
												'items' => [
													['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
													['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
												],
											],
										],
									],
								],
							],
						],
					]
				) ?>
			</section>
		</aside>

        <div class="content-wrapper">
			<section class="content-header">
				<?php if (isset($this->blocks['content-header'])) { ?>
					<h1><?= $this->blocks['content-header'] ?></h1>
				<?php } else { ?>
					<h1>
						<?php
						if ($this->title !== null) {
							echo \yii\helpers\Html::encode($this->title);
						} else {
							echo \yii\helpers\Inflector::camel2words(
								\yii\helpers\Inflector::id2camel($this->context->module->id)
							);
							echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
						} ?>
					</h1>
				<?php } ?>

				<?=
				Breadcrumbs::widget(
					[
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					]
				) ?>
			</section>
			<section class="content">
				<?= $content ?>
			</section>
		</div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Верия</b> 2.0
			</div>
			<strong>Copyright &copy; <?=date('Y');?> <a href="http://webelement.ru/">Web Element</a>.</strong> Все права защищены
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li class="active"><a href="#control-sidebar-home-tab" aria-expanded="true" data-toggle="tab"><i class="fa fa-home"></i></a></li>
				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Возможно пригодится</h3>
					<ul class='control-sidebar-menu'></ul>
				</div>
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<h3 class="control-sidebar-heading">Возможно пригодится</h3>
					<ul class='control-sidebar-menu'></ul>
				</div>
			</div>
		</aside>
		<div class='control-sidebar-bg'></div>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>


<?php /* ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Webelement <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() */?>
